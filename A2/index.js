let students = ["John", "Joe", "Jane", "Jessie"];

function addToEnd(arr, value) {
  if (typeof value != "string") {
    return "error - can only add strings to an array";
  } else {
    arr.push(value);
    return arr;
  }
}

function addToStart(arr, value) {
  if (typeof value != "string") {
    return "error - can only add strings to an array";
  } else {
    arr.unshift(value);
    return arr;
  }
}

function elementChecker(arr, value) {
  if (arr.length == 0) {
    return "error - passed in array is empty";
  } else {
    index = arr.indexOf(value);

    if (index === -1) {
      return false;
    } else {
      return true;
    }
  }
}

function checkAllStringsEnding(arr, value) {
  if (arr.length == 0) {
    return "error - array must NOT be empty";
  }
  if (typeof value != "string") {
    return "error - 2nd argument must be of data type string";
  }
  if (value.length != 1) {
    return "error - 2nd argument must be a single character";
  } else {
    let checker = [];
    arr.forEach((element) => {
      if (typeof element == "string") {
        checker.push("string");
      }
    });
    if (checker.length != arr.length) {
      return "error - all array elements must be string";
    } else {
        let checker = [];
        arr.forEach((element) => {
            if (element[element.length -1] == value) {
              checker.push("yes");
            }
          });
          if (checker.length != arr.length) {
            return false
          } else {
              return true
          }
    }
  }
}

console.log(students);
